package com.example.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@org.springframework.stereotype.Controller
public class Controller {
	
	@RequestMapping(value="/")
	public String home() {
		String this_page = "처음 페이지 입니다.";
		
		return "/index";
	}
	
}
